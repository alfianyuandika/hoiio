const jwt = require("jsonwebtoken");

class AuthController {
  async getToken(req, res) {
    try {
      const body = {
        id: req.user._id,
      };

      const token = jwt.sign(body, process.env.JWT_TOKEN, {
        expiresIn: "60d",
      });

      const name = req.body.name;
      const email = req.user.email;

      return res.status(200).json({
        message: `hello ${name}! your account has been created`,
        
      });
    } catch (e) {
      return res.status(500).json({
        message: "intenal server error",
        error: e,
      });
    }
  }


  

  async getIn(req, res) {
    try {
      const body = {
        id: req.user._id,
      };

      const token = jwt.sign(body, process.env.JWT_TOKEN, {
        expiresIn: "60d",
      });

      const name = req.user.name;
      const IP = req.clientIp;
      const email = req.user.email;

      return res.status(200).json({
        message: `welcome : ${name}! your IP address is : ${IP}`,
        
      });
    } catch (e) {
      return res.status(500).json({
        message: "intenal server error",
        error: e,
      });
    }
  }
}

module.exports = new AuthController();
