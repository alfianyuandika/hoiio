const express = require("express");
const passport = require("passport");

const authController = require("../controllers/authController");

const authValidator = require("../middlewares/validator/authValidator");


const auth = require("../middlewares/auth");

const router = express.Router();

router.post("/signup", authValidator.signup, auth.signup, authController.getToken);

router.post("/signin", auth.signin, authController.getIn);

module.exports = router;
