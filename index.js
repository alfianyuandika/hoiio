require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
const fs = require("fs");
const path = require("path");

const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");

const express = require("express");
var connect = require("connect");
var http = require("http");

var requestIp = require("request-ip");
const app = express();


app.use(mongoSanitize());

// Prevent XSS attact
app.use(xss());


const limiter = rateLimit({
  windowMs: 5 * 60 * 1000, // 1 mins
  max: 5,
});

app.use(limiter);


app.use(hpp());


app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);


app.use(cors());

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
} else {
  
  let accessLogStream = fs.createWriteStream(
    path.join(__dirname, "access.log"),
    {
      flags: "a",
    }
  );

  
  app.use(morgan("combined", { stream: accessLogStream }));
}

const authRoutes = require("./routes/authRoutes");

app.use(requestIp.mw());

app.use(connect());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/auth", authRoutes);


const PORT = 3000 || process.env.PORT;
app.listen(PORT, () => console.log(`server running on:${PORT}`));
